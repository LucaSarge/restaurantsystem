package controller;
import model.*;

public interface TableOperation {
	
	public void execute(NormalTable t);
	
	public void execute(VipTable t);
	
}
