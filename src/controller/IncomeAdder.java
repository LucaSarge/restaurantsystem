package controller;

public class IncomeAdder {
	
	private double income;
	
	public IncomeAdder(){
		this.income = 0;
	}
	
	public void addValue(double value){
		this.income = this.income + value;
	}
	
	public double getIncome(){
		return this.income;
	}

}
