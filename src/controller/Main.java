package controller;

import view.*;

public class Main {

	public static void main(String[] args) {

		// initialize spark-java server
		System.out.println("Server is starting...");
		ViewManager viewManager = new SimpleManager();
		try{
			viewManager.startServer();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
