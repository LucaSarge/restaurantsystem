package controller;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import model.NormalTable;
import model.Order;
import model.VipTable;

public class CountLastWeekOrderNormalTable implements TableOperation {

	
	private OrderCounter counter;
	
	public CountLastWeekOrderNormalTable(OrderCounter counter){
		this.counter = counter;
		
	}
	
	@Override
	public void execute(NormalTable t) {
		List<Order> orderList = t.getAllOrders();
		Date date = new Date();
		
		Calendar c = Calendar.getInstance();
		date.setDate(date.getDate()-(date.getDay()+1));
		c.setTime(date);
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());
		Date weekStart = c.getTime();
		weekStart.setDate(weekStart.getDate()-1);
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek()+6);
		Date weekEnd = c.getTime();
		Iterator<Order> orderIterator = orderList.iterator();
		while(orderIterator.hasNext()){
			Date temp = orderIterator.next().getDate();
			if(temp.after(weekStart) && temp.before(weekEnd)){
				counter.increment();
			}
		}

	}

	@Override
	public void execute(VipTable t) {
		// TODO Auto-generated method stub

	}

}
