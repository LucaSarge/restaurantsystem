package controller;
import model.*;

interface FreeTableObserver {
	
	public void handleNotification(Table t);
	
}
