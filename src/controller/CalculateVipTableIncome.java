package controller;
import java.util.Iterator;
import java.util.List;

import model.*;

class CalculateVipTableIncome implements TableOperation {
	
	private IncomeAdder adder;
	
	public CalculateVipTableIncome(IncomeAdder adder){
		this.adder = adder;
	}
	
	@Override
	public void execute(NormalTable t) {		
	}

	@Override
	public void execute(VipTable t) {
		List <Order> orderList = t.getAllOrders();
		Iterator<Order> orderIterator = orderList.iterator();
		while(orderIterator.hasNext()){
			Order currentOrder = orderIterator.next();
			List<OrderDetail> tempList = currentOrder.getDishDetail();
			Iterator<OrderDetail> dishIterator = tempList.iterator();
			while (dishIterator.hasNext()){
				OrderDetail detail = dishIterator.next();
				Dish d = detail.getDish();
				int quantity = detail.getQuantity();
				adder.addValue(d.getPrice()*quantity);
			}
			adder.addValue(t.getCoverCharge()*currentOrder.getNumberOfPersons());
		}
	}

}