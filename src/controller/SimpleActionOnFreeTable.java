package controller;

import model.Order;
import model.Table;

public class SimpleActionOnFreeTable implements FreeTableObserver {

	@Override
	public void handleNotification(Table t) {
		String number = t.getNumber();
		//notify new free table	
		System.out.println("table n°"+number+" is free");
	}

}
