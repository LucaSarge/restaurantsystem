package controller;

import java.util.Date;
import java.util.List;

import model.Order;
import model.OrderDetail;
import model.Table;

public class SimpleActionOnNewOrder implements NewOrderObserver {

	@Override
	public void handleNotification(Order o) {
		int personNumber = o.getNumberOfPersons();
		List<OrderDetail> orderDetail = o.getDishDetail();
		// notify new order
		System.out.println("new order for "+personNumber+" persons");
	}

	

}
