package controller;

public class OrderCounter {
	
	private int count;
	
	public OrderCounter(){
		this.count = 0;
	}
	
	public void increment(){
		count ++;
	}
	
	public int getCount(){
		return this.count;
	}
}
