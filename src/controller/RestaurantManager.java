package controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.*;

public class RestaurantManager {
	
	private Restaurant restaurant;
	private List<FreeTableObserver> freeTableObservers;
	private List<NewOrderObserver> newOrderObservers;
	
	public RestaurantManager(int restaurantID){
		this.restaurant = new Restaurant(restaurantID);
		this.freeTableObservers = new ArrayList<FreeTableObserver>();
		this.newOrderObservers = new ArrayList<NewOrderObserver>();
	}
	
	//-----------GetRestaurantInfo----------------
	
	public String getRestaurantName(){
		return this.restaurant.getName();
	}
	
	public int getRestaurantId(){
		return this.restaurant.getID();
	}
	
	
	//-----------GetRestaurantRoomsAndAreas----------------
	
	public List <Table> getAllTable(){
		return this.restaurant.getAllTables();
	}
	
	public Area getAreaByName(String name){
		return this.getAreaByName(name);
	}
	
	
	//-----------GetOrderAndMenuInfo----------------
	
	public Order getOrderById(int id){
		return this.restaurant.getOrder(id);
	}
	
	public Dish getDishByName(String name){
		List<Dish> menu = this.getMenu();
		Iterator<Dish> it = menu.iterator();
		while(it.hasNext()){
			Dish d = it.next();
			if(d.getName().equals(name)){
				return d;
			}
		}
		return null;
	}
	
	public List<Dish> getMenu(){
		return this.restaurant.getMenu();
	}
	
	
	//-----------CountMethod----------------
	
	public int countAllFreeTables(){
		int count = 0;
		List <Table> allTableList = this.getAllTable();
		Iterator<Table> tableIterator = allTableList.iterator();
		while (tableIterator.hasNext()){
			if(tableIterator.next().getIsFree()){
				count++;
			}
		}
		return count;
	}
	
	public int countAreaFreeTables(Area a){
		int count = 0;
		List <Room> roomList = a.getRooms();
		Iterator<Room> roomIterator = roomList.iterator();
		while (roomIterator.hasNext()){
			List<Table> tempList = roomIterator.next().getAllTables();
			Iterator<Table> tableIterator = tempList.iterator();
			while (tableIterator.hasNext()){
				if(tableIterator.next().getIsFree()){
					count++;
				}
			}
		}
		return count;
	}
	
	public int countUncompletedOrders(){
		int count = 0;
		Iterator<Order> orderIterator = this.restaurant.getAllOrders().iterator();
		while(orderIterator.hasNext()){
			if(!orderIterator.next().isCompleted()){
				count++;
			}
			
		}
		return count;
	}
	
	public int countLastWeekOrdersNormalTable(){
		OrderCounter counter = new OrderCounter();
		List<Table> tableList = this.getAllTable();
		CountLastWeekOrderNormalTable orderNT = new CountLastWeekOrderNormalTable(counter);
		Iterator<Table> tableIterator = tableList.iterator();
		while (tableIterator.hasNext()){
			tableIterator.next().executeOperation(orderNT);
		}
		return counter.getCount();
	}
	
	public int countLastWeekOrdersVipTable(){
		OrderCounter counter = new OrderCounter();
		List<Table> tableList = this.getAllTable();
		CountLastWeekOrderVipTable orderNT = new CountLastWeekOrderVipTable(counter);
		Iterator<Table> tableIterator = tableList.iterator();
		while (tableIterator.hasNext()){
			tableIterator.next().executeOperation(orderNT);
		}
		return counter.getCount();
	}

		
	//-----------IncomeMethod----------------
	
	public double calculateIncomeOfVipTable(){
		IncomeAdder adder = new IncomeAdder();
		List<Table> tableList = this.getAllTable();
		TableOperation calculateVipIncome = new CalculateVipTableIncome(adder);
		Iterator<Table> tableIterator = tableList.iterator();
		while (tableIterator.hasNext()){
			tableIterator.next().executeOperation(calculateVipIncome);
		}
		return adder.getIncome();
	}
	
	
	//------update Order and Tables---------
	
	public void completeOrder(int orderId){
		Order o = restaurant.getOrder(orderId);
		restaurant.completeOrder(o);
	}
	
	public void pay(int orderId){
		Order o = restaurant.getOrder(orderId);
		restaurant.pay(o);
		List<Table> tableList = restaurant.getAllTables();
		Table finalT = null;
		Iterator<Table> tableIterator = tableList.iterator();
		while(tableIterator.hasNext()){
			Table t = tableIterator.next();
			Iterator<Order> orderIterator = t.getAllOrders().iterator();
			while(orderIterator.hasNext()){
				Order order = orderIterator.next();
				if(order.getOrderID() == orderId)
					t.free();
					finalT=t;
			}
		}
		this.notifyAll(finalT);
	}
	
	
	//-----------addMethod----------------
	
	public void addArea(String areaName, String superiorAreaName){
		Area a = new AreaLeaf(areaName);
		this.restaurant.addArea(a, superiorAreaName);
	}
	
	public void addArea(String areaName){
		Area a = new AreaLeaf(areaName);
		this.restaurant.addArea(a);
	}
	
	public void addRoom(String roomName ,String areaName){
		Room r = new Room(roomName);
		Area a = this.restaurant.getArea(areaName);
		this.restaurant.addRoom(r, a);
	}
	
	public void addTable(String tableNumber, String roomName, boolean isVip){
		Table t;
		if(isVip)
			t = new VipTable(tableNumber, true);
		else
			t = new NormalTable(tableNumber, true);
		
		Room r = this.restaurant.getRoom(roomName);
		this.restaurant.addTable(t, r, isVip);
	}
	
	public void addOrder(String tableNumber, List<OrderDetail> orderDetails, String maidsName, int numberOfPersons){
		List<OrderDetail> orderDetailList = new ArrayList<OrderDetail>();
		List<Dish> menu = this.restaurant.getMenu();
		Iterator<OrderDetail> orderDetailsIterator = orderDetails.iterator();
		Iterator<Dish> menuIterator = menu.iterator();
		while (orderDetailsIterator.hasNext()){
			OrderDetail orderDetail = orderDetailsIterator.next();
			while(menuIterator.hasNext()){
				Dish menuDish = menuIterator.next();
				if(menuDish.getName().equals(orderDetail.getDish().getName())){
					orderDetailList.add(orderDetail);
				}
			}
		}
		Maids m = new Maids(maidsName);
		Table t = this.restaurant.getTable(tableNumber);
		Order o = new Order(orderDetailList, m, numberOfPersons, false, false);
		this.restaurant.addOrder(t, o);
		this.notifyAll(o);
	}
	
	public void addDish(String dishName, double price){
		Dish d = new Dish(dishName, price);
		this.restaurant.addDish(d);
	}
	
	public void addMaids(String maidsName){
		Maids m = new Maids(maidsName);
		this.restaurant.addMaids(m);
	}
	
	public void addNewOrderObserver(NewOrderObserver o){
		this.newOrderObservers.add(o);
	}
	
	public void addFreeTableObserver(FreeTableObserver o){
		this.freeTableObservers.add(o);
	}
	
	
	//-----------ObserverMethod----------------
	
	private void notifyAll(Table t){
		Iterator<FreeTableObserver> observerIterator = this.freeTableObservers.iterator();
		while(observerIterator.hasNext()){
			FreeTableObserver ob = observerIterator.next();
			ob.handleNotification(t);
		}
	}
	
	private void notifyAll(Order o){
		Iterator<NewOrderObserver> observerIterator = this.newOrderObservers.iterator();
		while(observerIterator.hasNext()){
			NewOrderObserver ob = observerIterator.next();
			ob.handleNotification(o);
		}
	}
	
}