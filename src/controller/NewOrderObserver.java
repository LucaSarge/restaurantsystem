package controller;
import model.*;

public interface NewOrderObserver {
	
	public void handleNotification(Order o);

	
}
