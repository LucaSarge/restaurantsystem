package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import model.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class ModelTest {
	
	private static Restaurant r;
	
	@BeforeClass
	public static void setUp(){
		r = new Restaurant(4);
	}

	@Test
	public void areaTest() {
		r.addArea(new AreaLeaf("AreaTest1"));
		AreaLeaf al1 = new AreaLeaf("ChildAreaTest1");
		r.addArea(al1, "AreaTest1");
		
		Area father = (AreaParent)r.getArea("AreaTest1");
		
		Area child = father.getChildrenAreas().get(0);
		
		assertEquals(child.getName(),al1.getName());
	}
	
	@Test
	public void roomTest() {
		AreaLeaf a = new AreaLeaf("ChildAreaTest1");
		Room room1 = new Room("RoomTest1");
		r.addRoom(room1, a);
		assertEquals(r.getArea(a.getName()).getRooms().get(0).getName(), room1.getName());
	}
	
	
	
	@Test
	public void dishTest(){
		Dish d = new Dish("Gnocchi al ragu",10.0);
		r.addDish(d);
		assertEquals(r.getMenu().get(0).getName(), d.getName());
		assertTrue(r.getMenu().get(0).getPrice() == d.getPrice());
	}
	

	@Test
	public void tableTest(){
		Room room = new Room("RoomModelTest2");
		Area area = new AreaLeaf("AreaModelTest2");
		Table t1 = new NormalTable(""+1,true);
		r.addArea(area);
		r.addRoom(room, area);
		r.addTable(t1, room, false);
		
		assertEquals(r.getRoom(room.getName()).getAllTables().get(0).getNumber(), t1.getNumber());
		assertTrue(r.getRoom(room.getName()).getAllTables().get(0).getCoverCharge() == 1.0);
		assertTrue(r.getRoom(room.getName()).getAllTables().get(0).getIsFree());
		
	}
	
	@Test
	public void orderTest(){
		Dish d = new Dish("Gnocchi al ragu",10.0);
		OrderDetail temp = new OrderDetail(d,1);
		List<OrderDetail> list = new ArrayList<OrderDetail>();
		list.add(temp);
		Order o = new Order(list,new Maids("Luca Sargenti"),1,false,false);
		r.addOrder(new NormalTable(""+1,false),o);
		
		assertEquals(((Dish)r.getTable(""+1).getAllOrders().get(0).getDishDetail().get(0).getDish()).getName(), temp.getDish().getName());
		assertEquals(r.getTable(""+1).getAllOrders().get(0).getDishDetail().get(0).getQuantity(), temp.getQuantity());
	}
	
	

}
