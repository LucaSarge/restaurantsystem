package test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import controller.NewOrderObserver;
import controller.RestaurantManager;
import controller.SimpleActionOnFreeTable;
import controller.SimpleActionOnNewOrder;
import model.Area;
import model.OrderDetail;
import model.Restaurant;
import model.Table;

public class RestaurantManagerTest {
	
	private static RestaurantManager rm;
	private static RestaurantManager rm2;
	
	@BeforeClass
	public static void setUp(){
		rm = new RestaurantManager(2);
		rm2 = new RestaurantManager(3);
		rm2.addNewOrderObserver(new SimpleActionOnNewOrder());
		rm2.addFreeTableObserver(new SimpleActionOnFreeTable());
	}

	@Test
	public void getAllTablesTest() {
		List<Table> tableList = rm.getAllTable();
		int count = tableList.size();
		//System.out.println("test getAllTablesTest: " +count);
		assertEquals(5, count);
	}
	
	@Test
	public void countAllFreeTablesTest() {
		int count = rm.countAllFreeTables();
		//System.out.println("test countAllFreeTablesTest: "+count);
		assertEquals(3, count);
	}
	
	@Test
	public void countAreaFreeTablesTest() {
		Restaurant r = new Restaurant(2);
		Area a = r.getArea("AreaRes2Parent1");
		int count = rm.countAreaFreeTables(a);
		//System.out.println("test countAreaFreeTablesTest: "+count);
		assertEquals(1, count);
	}

	@Test
	public void countUncompletedOrderTest() {
		int count = rm.countUncompletedOrders();
		//System.out.println("test countUncompletedOrderTest: "+ count);
		assertEquals(1, count);
	}
	
	@Test
	public void calculateincomeOfVipTableTest() {
		double income = rm.calculateIncomeOfVipTable();
		double trueIncome = 68;
		//System.out.println("test calculateincomeOfVipTableTest: "+ income);
		assertTrue(trueIncome==income);
	}
	

	@Test
	public void countLastWeekOrdersNormalTableTest() {
		int count = rm.countLastWeekOrdersNormalTable();
		//System.out.println("test countLastWeekOrdersNormalTableTest: "+count);
		assertEquals(1, count);
	}
	
	@Test
	public void countLastWeekOrderVipTableTest() {
		int count = rm.countLastWeekOrdersVipTable();
		//System.out.println("test countLastWeekOrderVipTableTest: "+count);
		assertEquals(2, count);
	}
	
	
	@Test
	public void newOrderObserverTest(){
		rm2.addOrder("88", new ArrayList<OrderDetail>(), "Giacomo Lombardi", 2);
	}
	
	@Test
	public void newFreeTableObserverTest(){
		rm2.pay(40);;
	}
	
}
