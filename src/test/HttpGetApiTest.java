package test;

import static org.junit.Assert.*;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.*;

import spark.utils.IOUtils;
import view.*;

public class HttpGetApiTest {
	
	private static ViewManager view;
	
	//---------------------------------------------------------------------------------
	// operation before and after testing
	//---------------------------------------------------------------------------------
	@BeforeClass
	public static void setUp() throws Exception {
		// this operation takes much time, need to charge all object structure
		view = new SimpleManager();
		view.startServer();
		Date init = new Date();
		long waitTime = (25)*1000;
		while((new Date()).getTime() - (init.getTime()) < waitTime){
			// do nothing
		}
	}

	@AfterClass
	public static void tearDown() throws Exception {
		view.stopServer();
	}
	
	
	//---------------------------------------------------------------------------------
	// testing all http-api methods
	//---------------------------------------------------------------------------------
	@Test
	public void getMenu() {
		TestResponse res = request("GET", "/menu");
		JsonObject json = res.json();
		assertEquals(200, res.status);
		JsonArray dishes = json.get("menu").getAsJsonArray();

		String dish0 = ((JsonObject)dishes.get(0)).get("dish").getAsString();
		String dish1 = ((JsonObject)dishes.get(1)).get("dish").getAsString();
		String dish2 = ((JsonObject)dishes.get(2)).get("dish").getAsString();
		String dish3 = ((JsonObject)dishes.get(3)).get("dish").getAsString();
		String dish4 = ((JsonObject)dishes.get(4)).get("dish").getAsString();
		String dish5 = ((JsonObject)dishes.get(5)).get("dish").getAsString();
		assertEquals("Gnocchi ai quattro formaggi", dish0);
		assertEquals("Gnocchi al tartufo", dish1);
		assertEquals("Tagliatelle al ragu", dish2);
		assertEquals("Tagliata di angus ai porcini", dish3);
		assertEquals("Tagliata di angus rucola e pachini", dish4);
		assertEquals("Tagliata di chianina rucola e pachini", dish5);
	}
	
	@Test
	public void compareLastWeekOrders(){
		TestResponse res = request("GET", "/compareLastWeekTable");
		assertEquals(200, res.status);
		JsonObject json = res.json();
		int normal = json.get("normal").getAsInt();
		int vip = json.get("vip").getAsInt();
		assertEquals(normal, 0);
		assertEquals(vip, 1);
	}
	
	@Test
	public void calculateIncomeOfVipTable(){
		TestResponse res = request("GET", "/calculateIncomeOfVipTable");
		assertEquals(200, res.status);
		JsonObject json = res.json();
		double income = json.get("income").getAsDouble();
		assertEquals(income, 68.0, 0.01); //third parameter is the delta
	}
	
	@Test
	public void countFreeTable(){
		TestResponse res = request("GET", "/countFreeTable");
		assertEquals(200, res.status);
		JsonObject json = res.json();
		int tables = json.get("freeTable").getAsInt();
		assertEquals(tables, 4);
	}
	
	@Test
	public void countUncompletedOrders(){
		TestResponse res = request("GET", "/countUncompletedOrders");
		assertEquals(200, res.status);
		JsonObject json = res.json();
		int uncompleted = json.get("uncompleted").getAsInt();
		assertEquals(uncompleted, 4);
	}
	
	@Test
	public void showOrder(){
		TestResponse res = request("GET", "/showOrder/22");
		assertEquals(200, res.status);
		JsonObject json = res.json();
		JsonArray order = json.get("order").getAsJsonArray();
		assertNotNull(order);
	}
	
	@Test
	public void completeOrder(){
		JsonObject data = new JsonObject();
		data.addProperty("id", 22);
		TestResponse res = request("POST", "/completeOrder", data);
		assertEquals(200, res.status);
		JsonObject json = res.json();
		boolean done = json.get("done").getAsBoolean();
		assert(done);
	}
	
	@Test
	public void pay(){
		JsonObject data = new JsonObject();
		data.addProperty("id", 22);
		TestResponse res = request("POST", "/pay", data);
		assertEquals(200, res.status);
		JsonObject json = res.json();
		boolean done = json.get("done").getAsBoolean();
		assert(done);
	}
	
	//---------------------------------------------------------------------------------
	// utilities for getting response
	//---------------------------------------------------------------------------------
	private TestResponse request(String method, String path) {
		try {
			URL url = new URL("http://localhost:4567" + path);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod(method);
			connection.setRequestProperty("restaurant", "1");
			connection.setDoOutput(true);
			connection.connect();
			String body = IOUtils.toString(connection.getInputStream());
			return new TestResponse(connection.getResponseCode(), body);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Sending request failed: " + e.getMessage());
			return null;
		}
	}
	
	private TestResponse request(String method, String path, JsonObject params){
		try {
			URL url = new URL("http://localhost:4567" + path);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod(method);
			connection.setRequestProperty( "Content-Type", "application/json");
			connection.setRequestProperty("restaurant", "1");
			connection.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream( connection.getOutputStream());
			wr.write( params.toString().getBytes() );
			connection.connect();
			String body = IOUtils.toString(connection.getInputStream());
			return new TestResponse(connection.getResponseCode(), body);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Sending request failed: " + e.getMessage());
			return null;
		}
	}

	private static class TestResponse {

		public final String body;
		public final int status;

		public TestResponse(int status, String body) {
			this.status = status;
			this.body = body;
		}
		
		public JsonObject json(){
			JsonParser jsonParser = new JsonParser();
			return (JsonObject)jsonParser.parse(body);
		}
	}

}
