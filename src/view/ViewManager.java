package view;

public interface ViewManager {
	
	public void startServer();
	
	public void stopServer();
	
}
