package view;

// using static due to many 'get' and 'post' methods to write
import static spark.Spark.*;

import java.util.*;
import com.google.gson.*;

import controller.RestaurantManager;
import model.*;

public class SimpleManager implements ViewManager{
	
	private String ipAddress = "localhost";
	private int port = 4567;
	private JsonObject json;
	
	private RestaurantManager restaurant;
	
	//------------------------------------------------------
	// constructors
	//------------------------------------------------------
	public SimpleManager(){
		// address and port as default
	}
	
	public SimpleManager(int port){
		this.port = port;
	}
	
	public SimpleManager(String address){
		this.ipAddress = address;
	}
	
	public SimpleManager(String address, int port){
		this.ipAddress = address;
		this.port = port;
	}
	
	
	//------------------------------------------------------
	// server methods (start and stop)
	//------------------------------------------------------
	public void startServer(){
		ipAddress(this.ipAddress);
		port(this.port);
		this.json = null;
		this.listen();
	}

	public void stopServer(){
		stop();
	}
	
	
	//------------------------------------------------------
	// routes
	//------------------------------------------------------
	private void listen(){
		
		// -------------------------------------------------
		// setup restaurant manager
		// -------------------------------------------------
		this.restaurant = new RestaurantManager(1); // example for one restaurant
		
		System.out.println("Server listen on "+this.ipAddress+" with port "+this.port);
		
		// -------------------------------------------------
		// set response type for all routes
		// -------------------------------------------------
		after((req, res) -> {
		  res.type("application/json");
		});
		
		// -------------------------------------------------
		// check restaurant name
		// -------------------------------------------------
		before((req, res) -> {
			boolean managed = false;
			try{
				int r = Integer.parseInt(req.headers("restaurant")); // not used for simple implementation of this problem
				if(this.restaurant.getRestaurantId() == r){
					managed = true;
				}
				if(!managed){
					halt(401, "restaurant is not managed");
				}
			}catch(NumberFormatException nfe){
				halt(401, "unable to parse current restaurant");
			}
		});
		
		// -------------------------------------------------
		// filter for POST body parsing
		// -------------------------------------------------
		before((req, res) -> {
			if(req.requestMethod().toLowerCase().equals("post")){
				try{
					if(! req.contentType().equals("application/json")){
						halt(401, "wrong content type");
					}
					// parse body
					JsonParser jsonParser = new JsonParser();
					this.json = (JsonObject)jsonParser.parse(req.body());
					
				}catch(Exception e){
					e.printStackTrace();
					halt(401, "unable to parse body of this request");
				}
			}
		});
		
		// -------------------------------------------------
		// write all routes
		// -------------------------------------------------
		get("/menu", (req, res) -> {
			String output = "";
			try{
				List<Dish> dishes = this.restaurant.getMenu();
				Iterator<Dish> it = dishes.iterator();
				while(it.hasNext()){
					Dish d = it.next();
					String product = d.getName();
					double price = d.getPrice();
					output = output + "\n\t{\n\t\t\"dish\": \""+product+"\",\n\t\t\"price\": "+price+"\n\t},";
				}
			}catch(Exception e){
				halt(401, "unable to get menu");
			}
			return "{\"menu\": ["+output+"\n]}";
		});
		
		post("/newOrder", (req, res) -> {
			List<OrderDetail> list = new ArrayList<OrderDetail>();
			try{
				// getting params from parsed json request
				String dishesParam = this.json.get("dishes").getAsString();
				String quantityParam = this.json.get("quantity").getAsString();
				String maids = this.json.get("maids").getAsString();
				String person = this.json.get("person").getAsString();
				String table = this.json.get("table").getAsString();
				
				// work with params
				String[] dishes = dishesParam.split(";");
				String[] quantity = quantityParam.split(";");
				if(dishes.length != quantity.length){
					throw new Exception("different size of input");
				}
				for(int i=0; i<dishes.length; i++){
					Dish d = this.restaurant.getDishByName(dishes[i].trim());
					int n = Integer.parseInt(quantity[i].trim());
					//Object[] obj = {d, n};
					OrderDetail obj = new OrderDetail(d, n);
					list.add(obj);
				}
				int numberOfPerson = Integer.parseInt(person);
				
				// creating order
				this.restaurant.addOrder(table, list, maids, numberOfPerson);
				
			}catch(Exception e){
				e.printStackTrace();
				return "{\"done\": false}";
			}
			return "{\"done\": true}";
		});
		
		get("/calculateIncomeOfVipTable", (req, res) -> {
			double income = 0.0;
			try{
				income = this.restaurant.calculateIncomeOfVipTable();
			}catch(Exception e){
				e.printStackTrace();
				halt(401, "error on getting income");
			}
			return "{\"income\": "+income+"}";
		});
		
		get("/compareLastWeekTable", (req, res) -> {
			int normal = this.restaurant.countLastWeekOrdersNormalTable();
			int vip = this.restaurant.countLastWeekOrdersVipTable();
			return "{\"normal\": "+normal+",\n\"vip\": "+vip+"}";
		});
		
		get("/countUncompletedOrders", (req, res) -> {
			int orders = 0;
			orders = this.restaurant.countUncompletedOrders();
			return "{\"uncompleted\": "+orders+"}";
		});
		
		get("/countFreeTable", (req, res) -> {
			int freeTables = 0;
			freeTables = this.restaurant.countAllFreeTables();
			return "{\"freeTable\": "+freeTables+"}";
		});
		
		get("/countFreeTable/:areaName", (req, res) -> {
			String areaName = req.params("areaName");
			int freeTables = 0;
			try{
				Area area = this.restaurant.getAreaByName(areaName);
				freeTables = this.restaurant.countAreaFreeTables(area);
			}catch(Exception e){
				halt(500, "selected area doesn't exist");
			}
			return "{\"freeTables\": "+freeTables+"}";
		});
		
		get("/showOrder/:id", (req, res) -> {
			String responseContent = "";
			try{
				int id = Integer.parseInt(req.params("id"));
				Order order = this.restaurant.getOrderById(id);
				List<OrderDetail> dishes = order.getDishDetail();
				Iterator<OrderDetail> it = dishes.iterator();
				while(it.hasNext()){
					OrderDetail current = it.next();
					Dish currentDish = current.getDish();
					String currentDishName = currentDish.getName();
					int currentQuantity = current.getQuantity();
					responseContent = responseContent + "\n\t{ \"dish\": \""+currentDishName+"\", \"quantity\": "+currentQuantity+"} ,";
				}
				responseContent = responseContent.substring(0, responseContent.length()-1); // cut last ,
			}catch(NumberFormatException nfe){
				halt(401, "order id must be a number");
			}catch(NullPointerException npe){
				halt(401, "selected order doesn't exist");
			}catch(Exception e){
				e.printStackTrace();
				halt(500, "internal server error");
			}
			return "{ \"order\": ["+responseContent+"\n]}";
		});
		
		post("/completeOrder", (req, res) -> {
			try{
				// getting params from parsed json request
				int orderId = this.json.get("id").getAsInt();
				this.restaurant.completeOrder(orderId);
			}catch(Exception e){
				e.printStackTrace();
				return "{\"done\": false}";
			}
			return "{\"done\": true}";
		});
		
		post("/pay", (req, res) -> {
			try{
				// getting params from parsed json request
				int orderId = this.json.get("id").getAsInt();
				this.restaurant.pay(orderId);
			}catch(Exception e){
				e.printStackTrace();
				return "{\"done\": false}";
			}
			return "{\"done\": true}";
		});
		
		post("/addArea", (req, res) -> {
			String name = null;
			try{
				name = this.json.get("name").getAsString();
			}catch(Exception e){
				halt(401, "name param is required");
			}
			String fatherName = null;
			try{
				fatherName = this.json.get("father").getAsString();
			}catch(Exception e){
				// do nothing, father isn't specified
			}finally{
				try{
					if(fatherName != null){
						this.restaurant.addArea(name, fatherName);
					}else{
						this.restaurant.addArea(name);
					}
					return "{\"done\": true}";
				}catch(Exception e){
					return "{\"done\": false}";
				}
			}
		});
		
		post("/addRoom", (req, res) -> {
			String roomName = null;
			String areaName = null;
			try{
				roomName = this.json.get("name").getAsString();
				areaName = this.json.get("area").getAsString();
			}catch(Exception e){
				halt(401, "needed params: (name, area)");
			}
			try{
				this.restaurant.addRoom(roomName, areaName);
				return "{\"done\": true}";
			}catch(Exception e){
				return "{\"done\": false}";
			}
		});
		
		post("/addTable", (req, res) -> {
			String roomName = null;
			String tableNumber = null;
			boolean isVip = false;
			try{
				roomName = this.json.get("roomName").getAsString();
				tableNumber = this.json.get("number").getAsString();
				isVip = (this.json.get("isVip").getAsString()).equals("true");
			}catch(Exception e){
				halt(401, "needed params: (roomName, number, isVip)");
			}
			try{
				this.restaurant.addTable(tableNumber, roomName, isVip);
				return "{\"done\": true}";
			}catch(Exception e){
				return "{\"done\": false}";
			}
		});
		
	}
	
}
