package model;

import java.util.ArrayList;
import java.util.List;

import controller.TableOperation;

public abstract class AbstractTable implements Table {
	
	protected String number;
	protected boolean isFree;
	protected List<Order> orderList;
	
	public AbstractTable(String number,boolean isFree){
		this.number = number;
		this.isFree = isFree;
		this.orderList = new ArrayList<Order>();
	}
	
	public String getNumber(){
		return this.number;
	}
	
	public boolean getIsFree(){
		return this.isFree;
	}
	
	
	public void addOrder(Order o){
		this.orderList.add(o);
	}
	
	public void occupy(){
		this.isFree = false;
	}
	
	public void free(){
		this.isFree = true;
	}
	
	public List<Order> getAllOrders(){
		return this.orderList;
	}
	
	public void resetOrders(){
		this.orderList.clear();
	}
	
	public abstract Table returnObject();
	
	public abstract double getCoverCharge();
	
	public abstract void executeOperation(TableOperation o);
}
