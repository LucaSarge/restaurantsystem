package model;

import java.util.List;

public interface Area {
	
	public String getName();

	public void addRoom(Room r);
	
	public List<Area> getChildrenAreas();
	
	public List<Room> getRooms();
	
	public Area returnObject();
	
	public void resetRooms();
	
}
