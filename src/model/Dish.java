package model;

public class Dish {
	
	private String name;
	private String description="";
	private double price;
	
	public Dish(String name,double price){
		this.name = name;
		this.price = price;
	}
	
	public void setDescription(String desc){
		this.description = desc;
	}
	
	public String getName(){
		return this.name;
	}

	public String getDescription(){
		return this.description;
	}
	
	public double getPrice(){
		return this.price;
	}
	
	public void setPrice(double price){
		this.price = price;
	}
}
