package model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AreaParent extends AbstractArea {
	
	private List<Area> childAreas;
	
	public AreaParent(String name, List<Area> childAreas){
		super(name);
		this.childAreas = childAreas;
	}
	
	public List<Area> getChildrenAreas(){
		List<Area> list = new ArrayList<Area>();
		list.addAll(this.childAreas);
		Iterator<Area> it = this.childAreas.iterator();
		while(it.hasNext()){
			Area temp = it.next();
			list.addAll(temp.getChildrenAreas());
		}
		return list;
	}


	public Area returnObject(){
		return this;
	}
}
