package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseConnection {
	
	private Connection createConnection(){
		Connection c = null;
		try {
			Class.forName("org.postgresql.Driver");
			//c = DriverManager.getConnection("jdbc:postgresql://82.49.33.33:35614/Restaurant","postgres", "postgres");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Restaurant","postgres", "postgres");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			System.exit(0);
		}
		return c;
	}
	
	//INSERT OPERATION
	public void updateQuery(String sqlQuery){
		Connection c = createConnection();
		try {
			Statement stat = c.createStatement();
			stat.executeUpdate(sqlQuery);
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Statement addOrderQuery(){
		Connection c = createConnection();
		try {
			Statement stat = c.createStatement();
			return stat;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//SELECT OPERATION
	public ResultSet executeQuery(String sqlQuery){
		Connection c = createConnection();
		ResultSet result = null;
		try {
			Statement stat = c.createStatement();
			result = stat.executeQuery(sqlQuery);
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}

