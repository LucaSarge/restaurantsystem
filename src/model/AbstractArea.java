package model;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractArea implements Area {

	protected String name;
	protected List<Room> rooms;
	
	public AbstractArea(String name){
		this.name = name;
		this.rooms = new ArrayList<Room>();
	}
	
	public void addRoom(Room r) {
		this.rooms.add(r);
	}

	public String getName() {
		return this.name;
	}
	
	public List<Room> getRooms(){
		return this.rooms;
	}
	
	public void resetRooms(){
		this.rooms.clear();
	}
	
	public abstract List<Area> getChildrenAreas();
	
	public abstract Area returnObject();

}
