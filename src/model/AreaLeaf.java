package model;

import java.util.ArrayList;
import java.util.List;

public class AreaLeaf extends AbstractArea {
	
	public AreaLeaf(String name){
		super(name);
	}

	public Area returnObject(){
		return this;
	}

	@Override
	public List<Area> getChildrenAreas() {
		return new ArrayList<Area>();
	}

}
