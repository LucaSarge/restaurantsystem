package model;

public class OrderDetail {
	
	private Dish dish;
	private int quantity;
	
	public OrderDetail(Dish d,int q){
		this.dish=d;
		this.quantity = q;
	}
	
	public Dish getDish(){
		return this.dish;
	}
	
	public int getQuantity(){
		return this.quantity;
	}

}
