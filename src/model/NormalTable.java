package model;
import controller.TableOperation;
public class NormalTable extends AbstractTable{
	
	private final double coverCharge = 1;

	public NormalTable(String number,boolean isFree) {
		super(number,isFree);
	}

	public double getCoverCharge(){
		return coverCharge;
	}
	
	public void executeOperation(TableOperation o) {
		o.execute(this);
	}
	
	public Table returnObject(){
		return this;
	}
}
