package model;

import controller.TableOperation;


public class VipTable extends AbstractTable{
	
	private final double coverCharge = 2;
	
	public VipTable(String number,boolean isFree) {
		super(number,isFree);
	}

	public double getCoverCharge(){
		return coverCharge;
	}
	public void executeOperation(TableOperation o) {
		o.execute(this);
	}
	
	public Table returnObject(){
		return this;
	}
}

