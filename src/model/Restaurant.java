package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Restaurant {

	private int restaurantID;
	private String name;
	private String address;
	private String telephone;
	private String email;
	private String director;
	private List<Area> areas;
	private DatabaseConnection dbCon;
	
	public Restaurant(int restaurantID){
		this.restaurantID = restaurantID;
		this.dbCon = new DatabaseConnection();
		String queryToLoad = "SELECT * FROM restaurant WHERE restaurant_id = "+this.restaurantID;
		ResultSet result = this.dbCon.executeQuery(queryToLoad);
		try {
			result.next();
			this.name = result.getString("name");
			this.address = result.getString("address");
			this.telephone = result.getString("telephone");
			this.email = result.getString("email");
			this.director = result.getString("director");
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		updateAll();
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getID(){
		return this.restaurantID;
	}
	
	//-----------addMethod-----------------
	
	public void addArea(Area a){
		String queryToAdd = "INSERT INTO area (name,restaurant) VALUES " + "('"+a.getName()+"', '"+this.restaurantID+"')";
		this.dbCon.updateQuery(queryToAdd);
		updateAll();
	}
	
	public void addArea(Area a, String superiorAreaName){
		String queryToAdd = "INSERT INTO area (name,superior_area,restaurant) VALUES " + "('"+a.getName()+"', '"+getAreaIDByName(superiorAreaName) + "', '" + this.restaurantID+"')";
		this.dbCon.updateQuery(queryToAdd);
		updateAll();
	}
	
	public void addRoom(Room r, Area a){
		String addRoomQuery = "INSERT INTO room (area,name) VALUES " + "('" + getAreaIDByName(a.getName()) + "' , '" + r.getName() +"')";
		this.dbCon.updateQuery(addRoomQuery);
		updateAreaRoomList(getArea(a.getName()));
	}
	
	public void addTable(Table t,Room r, boolean isVip){ 
		String addTableQuery;
		if(isVip){
			addTableQuery ="INSERT INTO \"table\" (number,room,is_free,is_vip) VALUES " + 
							"('" + Integer.parseInt(t.getNumber()) + "' , '" + getRoomIDByName(r.getName()) + "' , '" + t.getIsFree() + "' , 'true')";
		}else{
			addTableQuery ="INSERT INTO \"table\" (number,room,is_free,is_vip) VALUES " + 
							"('" + Integer.parseInt(t.getNumber()) + "' , '" + getRoomIDByName(r.getName()) + "' , '" + t.getIsFree() + "' , 'false')";
		}
		
		this.dbCon.updateQuery(addTableQuery);
		updateRoomTableList(getRoom(r.getName()));
	}
	
	public void addOrder(Table t, Order o){
		String addOrder ="INSERT INTO \"order\" (maids,\"table\",number_of_person) VALUES "+ "('"+getMaidsIDByName(o.getMaids().getName())+"',"+getTableIDByNumber(t.getNumber())+","+ o.getNumberOfPersons()+ ")";
		Statement stat = this.dbCon.addOrderQuery();
		int orderId = -1;
		try {
			stat.executeUpdate(addOrder, Statement.RETURN_GENERATED_KEYS);
			ResultSet result = stat.getGeneratedKeys();
			result.next();
			orderId = result.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String addOrderDetail ="";
		Iterator<OrderDetail> it = o.getDishDetail().iterator();
		while(it.hasNext()){
			OrderDetail temp = it.next();
			addOrderDetail = "INSERT INTO order_detail (\"order\",quantity,dish)  VALUES ("+orderId+ " , "+temp.getQuantity() +" , "+ getDishIDByName(temp.getDish().getName())+")";
			this.dbCon.updateQuery(addOrderDetail);
		}
		occupyTable(t);
		updateTableOrderList(getTable(t.getNumber()));
	}
	
	public void addDish(Dish d){
		String addDishQuery;
		if(getDish(d.getName())==0){
			if(d.getDescription().equals("")){
				addDishQuery= "INSERT INTO dish (name) VALUES " + "('"+d.getName()+"')";
			}else{
				addDishQuery= "INSERT INTO dish (name,description) VALUES " + "('"+d.getName()+"','"+d.getDescription()+"')";
			}
			this.dbCon.updateQuery(addDishQuery);
		}
		
		String addToMenu = "INSERT INTO menu_dishes (menu,dish,price) VALUES ("+this.restaurantID+","+getDishIDByName(d.getName())+","+d.getPrice()+")";
		this.dbCon.updateQuery(addToMenu);
	}
	
	public void addMaids(Maids m){
		String addMaids= "INSERT INTO maids (name) VALUES " + "('"+m.getName()+"')";
		this.dbCon.updateQuery(addMaids);
	}
	
	
	//---------GetMethod--------
	//---------readFromObject
	public String getRestaurantInfo(){
		return "Restaurant Name: " + this.name + "\n\r" + "Address: " + this.address + "\n\r" + "Telephone: " + this.telephone + "\n\r" 
				+ "Email: " + this.email + "\n\r" + "Director: " + this.director + "\n\r";
	}
	
	public List<Area> getAllRootAreas(){
		return this.areas;
	}
	
	public List<Area> getAllAreas(){
		Iterator<Area> it = this.areas.iterator();
		List<Area> areaList = new ArrayList<Area>();
		while(it.hasNext()){
			Area temp = it.next();
			areaList.add(temp);
			areaList.addAll(temp.getChildrenAreas());
		}
		return areaList;
	}
	
	public Area getArea(String name){
		Iterator<Area> it = this.getAllAreas().iterator();
		while(it.hasNext()){
			Area temp = it.next();
			if(temp.getName().equals(name)){
				return temp;
			}
		}
		return null;
	}
	
	
	public List<Room> getAllRooms(){
		Iterator<Area> it = this.getAllAreas().iterator();
		List<Room> list = new ArrayList<Room>();
		while(it.hasNext()){
			list.addAll(it.next().getRooms());
		}
		return list;
	}
	

	
	public Room getRoom(String name){
		List<Room> rooms = getAllRooms();
		Iterator<Room> it = rooms.iterator();
		while(it.hasNext()){
			Room temp = it.next();
			if(temp.getName().equals(name))
				return temp.returnObject();
		}
		return null;
	}
	
	public List<Table> getAllTables(){
		List<Table> tables = new ArrayList<Table>();
		List<Room> rooms = getAllRooms();
		Iterator<Room> itRoom = rooms.iterator();
		while(itRoom.hasNext()){
			Room temp = itRoom.next();
			List<Table> tablesRoom = temp.getAllTables();
			Iterator<Table> itTable = tablesRoom.iterator();
			while(itTable.hasNext()){
				tables.add(itTable.next());
			}
		}
		return tables;
	}
	
	public Table getTable(String number){
		List<Table> tables = getAllTables();
		Iterator<Table> it = tables.iterator();
		while(it.hasNext()){
			Table temp = it.next();
			if(temp.getNumber().equals(number))
				return temp.returnObject();
		}
		return null;
	}
	
	public Table getOrderTable(Order o){
		List<Table> tables = getAllTables();
		Iterator<Table> itTable = tables.iterator();
		while(itTable.hasNext()){
			Table temp = itTable.next();
			List<Order> tableOrders = temp.getAllOrders();
			Iterator<Order> itOrder = tableOrders.iterator();
			while(itOrder.hasNext()){
				if(itOrder.next().getOrderID() == o.getOrderID())
					return temp;
			}
		}
		return null;
	}
	
	public List<Order> getAllOrders(){
		List<Order> orders = new ArrayList<Order>();
		List<Table> tables = getAllTables();
		Iterator<Table> itTable = tables.iterator();
		while(itTable.hasNext()){
			Table temp = itTable.next();
			List<Order> orderTable = temp.getAllOrders();
			Iterator<Order> itOrder = orderTable.iterator();
			while(itOrder.hasNext()){
				orders.add(itOrder.next());
			}
		}
		return orders;
	}
	
	public Order getOrder(int orderID){
		List<Order> orders = getAllOrders();
		Iterator<Order> it = orders.iterator();
		while(it.hasNext()){
			Order temp = it.next();
			if(temp.getOrderID()==orderID)
				return temp.returnObject();
		}
		return null;
	} 
	
	
	//------readFromDatabase
	
	private int getAreaIDByName(String areaName){
		String getAreaIdQuery = "SELECT area_id FROM area WHERE name  = '"+areaName+"' and restaurant = "+ this.restaurantID;
		ResultSet result = this.dbCon.executeQuery(getAreaIdQuery);	
		int areaId = -1;
		try {
			result.next();
			areaId = Integer.parseInt(result.getString("area_id"));
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return areaId;
	}
	
	private int getRoomIDByName(String name){
		int roomId = -1;
		String roomIdQuery = "SELECT room_id FROM room,area WHERE room.area = area.area_id and room.name = '"+ name +"' and restaurant = "+this.restaurantID;
		ResultSet result = this.dbCon.executeQuery(roomIdQuery);
		try{
			result.next();
			roomId = Integer.parseInt(result.getString("room_id"));
		}catch(SQLException e){
			e.printStackTrace();
		}
		return roomId;
	}
	
	private int getTableIDByNumber(String number){
		int tableID=-1;
		String tableIdQuery = "SELECT table_id FROM \"table\",room,area WHERE \"table\".number="+Integer.parseInt(number)+" and \"table\".room=room.room_id and room.area = area.area_id and restaurant = "+this.restaurantID;
		ResultSet result = this.dbCon.executeQuery(tableIdQuery);
		try{
			result.next();
			tableID = Integer.parseInt(result.getString("table_id"));
		}catch(SQLException e){
			e.printStackTrace();
		}
		return tableID;
	}
	
	private int getMaidsIDByName(String name){
		int maidsID = -1;
		String maidsIdQuery = "SELECT maids_id FROM maids WHERE  name = '"+name+"'";
		ResultSet result = this.dbCon.executeQuery(maidsIdQuery);
		try{
			result.next();
			maidsID = Integer.parseInt(result.getString("maids_id"));
		}catch(SQLException e){
			e.printStackTrace();
		}
		return maidsID;
	}
	
	private int getDishIDByName(String name){
		int dishID = -1;
		String dishIdQuery = "SELECT dish_id FROM dish WHERE name = '"+name+"'";
		ResultSet result = this.dbCon.executeQuery(dishIdQuery);
		try{
			result.next();
			dishID = Integer.parseInt(result.getString("dish_id"));
		}catch(SQLException e){
			e.printStackTrace();
		}
		return dishID;
	}
	
	private int getDish(String name){
		String dishIdQuery = "SELECT COUNT(*) as count FROM dish WHERE name = '"+name+"'";
		ResultSet result = this.dbCon.executeQuery(dishIdQuery);
		int found = -1;
		try{
			result.next();
			found  = result.getInt("count");
		}catch(SQLException e){
			e.printStackTrace();
		}
		return found;
	}
	
	public List<Dish> getMenu(){
		ArrayList<Dish> menu = new ArrayList<Dish>();
		String menuQuery = "SELECT dish.name, price FROM dish,menu_dishes, menu WHERE dish.dish_id = menu_dishes.dish and menu_dishes.menu = menu.menu_id and menu.restaurant = "+ this.restaurantID;
		ResultSet result = this.dbCon.executeQuery(menuQuery);
		try{
			while(result.next()){
				String price = result.getString("price").replaceAll(",", ".");
				price = price.substring(0, price.length()-1);
				Dish d = new Dish(result.getString("name"),Double.parseDouble(price));
				menu.add(d);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return menu;
	}
	
	//update Order and Tables
	
	private void occupyTable(Table t){
		getTable(t.getNumber()).occupy();
		String occupyQuery = "UPDATE \"table\" SET is_free=false WHERE table_id=" +getTableIDByNumber(t.getNumber()) ;
		this.dbCon.updateQuery(occupyQuery);
	}
	
	private void freeTable(Table t){
		getTable(t.getNumber()).free();
		String freeQuery = "UPDATE \"table\" SET is_free=true WHERE table_id=" +getTableIDByNumber(t.getNumber()) ;
		this.dbCon.updateQuery(freeQuery);
	}
	
	public void completeOrder(Order o){
		getOrder(o.getOrderID()).completeOrder();
		String completeOrderQuery = "UPDATE \"order\" SET is_completed=true WHERE order_id=" +o.getOrderID() ;
		this.dbCon.updateQuery(completeOrderQuery);
	}
	
	public void pay(Order o){
		getOrder(o.getOrderID()).payOrder();
		String completeOrderQuery = "UPDATE \"order\" SET is_paid=true WHERE order_id=" +o.getOrderID() ;
		this.dbCon.updateQuery(completeOrderQuery);
		freeTable(getOrderTable(o));
	}
	
	//---------------updateListMethod-------------
	
	private void updateAll(){
		updateAreaList();
		updateRoomList();
		updateTableList();
		updateOrderList();
	}
	
	private void updateAreaList(){
		this.areas = new ArrayList<Area>();
		String getRootAreasQuery = "SELECT * FROM area WHERE superior_area IS NULL and restaurant = "+this.restaurantID;
		String getAllAreasQuery = "SELECT * FROM area WHERE restaurant = "+this.restaurantID;
		ResultSet result1 = this.dbCon.executeQuery(getRootAreasQuery);
		ArrayList<Area> childAreaList;
		try{		
			while(result1.next()){
				String areaId1 = result1.getString("area_id");
				String name = result1.getString("name");
				childAreaList = new ArrayList<Area>();
				boolean hasChild = false;
				ResultSet result2 = this.dbCon.executeQuery(getAllAreasQuery);
				while(result2.next()){
					String areaId2 = result2.getString("superior_area");
					if(areaId1.equals(areaId2)){
						hasChild = true;
						String childName = result2.getString("name");
						AreaLeaf areaChild = new AreaLeaf(childName);
						childAreaList.add(areaChild);
					}else{
						//do nothing
					}
				}
				Area toAdd;
				if(hasChild){
					toAdd = new AreaParent(name,updateChildren(childAreaList));
				}else{
					toAdd = new AreaLeaf(name);
				}
				this.areas.add(toAdd);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	private List<Area> updateChildren(List<Area> childList){
		ArrayList<Area> updatedChildList = new ArrayList<Area>();
		Iterator<Area> it = childList.iterator();
		while(it.hasNext()){
			Area current = it.next();
			String currentAreaId = String.valueOf(getAreaIDByName(current.getName()));
			String currentAreaName = current.getName();
			String getAllAreasQuery = "SELECT * FROM area WHERE superior_area IS NOT NULL and restaurant = "+this.restaurantID;
			ResultSet result = this.dbCon.executeQuery(getAllAreasQuery);
			ArrayList<Area> childAreaList = new ArrayList<Area>();
			boolean hasChild = false;
			try{      
				while(result.next()){
					String areaResId = result.getString("superior_area");
					if(areaResId.equals(currentAreaId)){
						hasChild = true;
						String childName = result.getString("name");
						AreaLeaf areaChild = new AreaLeaf(childName);
						childAreaList.add(areaChild);
					}else{
						//do nothing
					}
				}
				Area toAdd;
				if(hasChild){
					toAdd = new AreaParent(currentAreaName,updateChildren(childAreaList));;
					//System.out.println(currentAreaName +" � parent");
				}else{
					toAdd = new AreaLeaf(currentAreaName);
					//System.out.println(currentAreaName +" � leaf");
				}
				updatedChildList.add(toAdd);
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		return updatedChildList;
	}	
	
	private void updateRoomList(){
		Iterator<Area> it = this.areas.iterator();
		while(it.hasNext()){
			Area a = it.next();
			updateAreaRoomList(a);
		}
	}
	
	private void updateAreaRoomList(Area a){
		a.resetRooms();
		String roomLoadQuery = "SELECT room.name FROM room,area WHERE room.area = area.area_id and area_id = '"+ getAreaIDByName(a.getName()) +"' and restaurant = "+this.restaurantID;
		ResultSet result = this.dbCon.executeQuery(roomLoadQuery);
		try{
			while(result.next()){
				String roomName = result.getString("name");
				Room room = new Room(roomName);
				a.addRoom(room);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		Iterator<Area> it1 = a.getChildrenAreas().iterator();
		while(it1.hasNext()){
			Area areaChild = it1.next();
			updateAreaRoomList(areaChild);
		}
	}
	
	private void updateTableList(){
		Iterator<Area> it = this.areas.iterator();
		while(it.hasNext()){
			Area a = it.next();
			updateAreaTableList(a);
		}
	}
	
	private void updateAreaTableList(Area a){ 
		List<Room> areaRoomList = a.getRooms();
		Iterator<Room> roomIt = areaRoomList.iterator();
		while(roomIt.hasNext()){
			Room temp = roomIt.next();
			updateRoomTableList(temp);
		}
		Iterator<Area> it1 = a.getChildrenAreas().iterator();
		while(it1.hasNext()){
			Area areaChild = it1.next();
			updateAreaTableList(areaChild);
		}
	}
	
	private void updateRoomTableList(Room rm){
		rm.resetTables();
		String roomLoadQuery = "SELECT \"table\".number,\"table\".is_free, \"table\".is_vip FROM \"table\",room,area WHERE room.area = area.area_id" +" and room.room_id = '"+ getRoomIDByName(rm.getName()) +"' and \"table\".room = room.room_id and restaurant = "+this.restaurantID;
		ResultSet result = this.dbCon.executeQuery(roomLoadQuery);
		try{
			while(result.next()){
				String tableNumber = result.getString("number");
				boolean isFree = result.getBoolean("is_free");
				boolean isVip = result.getBoolean("is_vip");
				Table table;
				if(isVip){
					table = new VipTable(tableNumber,isFree);
				}else{
					table = new NormalTable(tableNumber,isFree);
				}
				rm.addTable(table);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	private void updateOrderList(){
		Iterator<Area> it = this.areas.iterator();
		while(it.hasNext()){
			Area a = it.next();
			updateAreaOrderList(a);
		}
	}
	
	private void updateAreaOrderList(Area a){
		List<Room> areaRoomList = a.getRooms();
		Iterator<Room> roomIt = areaRoomList.iterator();
		while(roomIt.hasNext()){
			Room temp = roomIt.next();
			updateRoomOrderList(temp);
		}
		Iterator<Area> it1 = a.getChildrenAreas().iterator();
		while(it1.hasNext()){
			Area areaChild = it1.next();
			updateAreaOrderList(areaChild);
		}
	}
	
	private void updateRoomOrderList(Room r){
		List<Table> tableList = r.getAllTables();
		Iterator<Table> it = tableList.iterator();
		while(it.hasNext()){
			updateTableOrderList(it.next());
		}
	}
	
	private void updateTableOrderList(Table t){
		t.resetOrders();
		String orderQuery = "SELECT \"order\".order_id ,\"order\".number_of_person, \"order\".is_completed, \"order\".is_paid ,\"order\".date , maids.name FROM \"order\",\"table\",room,area,maids WHERE "
				+ " \"order\".\"table\" = \"table\".table_id and \"table\".number = "+t.getNumber()+" and room.room_id = \"table\".room and maids.maids_id = \"order\".maids and room.area=area.area_id and area.restaurant = "+this.restaurantID;
		ResultSet result = this.dbCon.executeQuery(orderQuery);
		try{
			while(result.next()){
				String orderId = result.getString("order_id");
				int numberOfPerson = result.getInt("number_of_person");
				boolean isCompleted = result.getBoolean("is_completed");
				boolean isPaid = result.getBoolean("is_paid");
				Date date = result.getDate("date");
				String maids = result.getString("name");
				Maids m = new Maids(maids);
				
				String orderDetailQuery = "SELECT dish.name, order_detail.quantity, menu_dishes.price  "
						+ " FROM order_detail, \"order\", dish, menu_dishes "
						+" WHERE \"order\".order_id = order_detail.\"order\" and dish.dish_id = order_detail.dish and menu_dishes.dish = dish.dish_id and \"order\".order_id = " + Integer.parseInt(orderId)
						+ " and menu_dishes.menu = " +this.restaurantID;

				ResultSet resultDetail = this.dbCon.executeQuery(orderDetailQuery);
				List<OrderDetail> orderDishList = new ArrayList<OrderDetail>();
				while(resultDetail.next()){
					String price = resultDetail.getString("price").replaceAll(",", ".");
					price = price.substring(0, price.length()-1);
					Dish d = new Dish(resultDetail.getString("name"),Double.parseDouble(price));
					int quantity = Integer.parseInt(resultDetail.getString("quantity"));
					OrderDetail temp = new OrderDetail(d,quantity);
					orderDishList.add(temp);
				}
				Order ord = new Order(orderDishList,m,numberOfPerson,isCompleted,isPaid);
				ord.setOrderID(Integer.parseInt(orderId));
				ord.setDate(date);
				t.addOrder(ord);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
}
