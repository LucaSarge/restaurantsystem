package model;

import java.util.List;

import controller.TableOperation;

public interface Table {
	
	public String getNumber();
	
	public boolean getIsFree();
	
	public void addOrder(Order o);
	
	public void occupy();
	
	public void free();
	
	public List<Order> getAllOrders();
	
	public double getCoverCharge();
	
	public Table returnObject();
	
	public void resetOrders();
	
	public void executeOperation(TableOperation o);
	
}
