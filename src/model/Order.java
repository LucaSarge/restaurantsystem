package model;

import java.util.Date;
import java.util.List;

public class Order {
	
	private int orderID;
	private Date date;
	private List<OrderDetail> dishes; 
	private Maids maids;
	private int numberOfPerson;
	private boolean isCompleted;
	private boolean isPaid;
	
	public Order(List<OrderDetail> d,Maids m,int numberOfPerson,boolean isCompleted,boolean isPaid){
		this.dishes = d;
		this.maids = m;
		this.numberOfPerson = numberOfPerson;
		this.isCompleted = isCompleted;
		this.isPaid = isPaid;
	}
	
	public void setDate(Date date){
		this.date = date;
	}
	
	public Date getDate(){
		return this.date;
	}
	
	public void setOrderID(int ID){
		this.orderID = ID;
	}
	
	public int getOrderID(){
		return this.orderID;
	}
	
	public void completeOrder(){
		this.isCompleted = true;
	}
	
	public boolean isCompleted(){
		return this.isCompleted;
	}
	
	public void payOrder(){
		this.isPaid= true;
	}
	
	public boolean isPaid(){
		return this.isPaid;
	}
	
	public Maids getMaids(){
		return this.maids;
	}
	
	public List<OrderDetail> getDishDetail(){
		return this.dishes;
	}
	
	public int getNumberOfPersons(){
		return this.numberOfPerson;
	}
	
	public Order returnObject(){
		return this;
	}
	
}
