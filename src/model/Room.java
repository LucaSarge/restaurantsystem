package model;

import java.util.ArrayList;
import java.util.List;

public class Room {

	private List<Table> tables;
	private String name;
	
	public Room(String name){
		this.name = name;
		this.tables = new ArrayList<Table>();
	}
	
	protected void addTable(Table t){
		this.tables.add(t);
	}
	
	public List<Table> getAllTables(){
		return this.tables;
	}
	
	public String getName(){
		return this.name;
	}
	
	public Room returnObject(){
		return this;
	}
	
	public void resetTables(){
		this.tables.clear();
	}
	
}
